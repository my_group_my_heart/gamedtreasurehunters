import 'dart:io';
import 'Article.dart';
import 'Craft.dart';
import 'DeadEnd.dart';
import 'Jackpot.dart';
import 'Magma.dart';
import 'Player.dart';

class Map {
  int _width = 10;
  int _height = 10;
  var jackpot = Jackpot(2, 9, 5);
  Player? player;
  Magma? magma;

  var cle = List<Article>.filled(20, Article(0, 0, ""), growable: false);

  int count = 0;
  Craft? craft;

  Map(int _width, int _height) {
    this._width;
    this._height;
  }
  void addArticle(Article article) {
    this.cle[count] = article; //article;
    count++;
  }

  bool isJackpot(int x, int y) {
    return jackpot.isOn(x, y);
  }

  void setPlayer(Player player) {
    this.player = player;
    addArticle(player);
  }

  void setCraft(Craft craft) {
    this.craft = craft;
    addArticle(craft);
  }

  void setMagma(Magma magma) {
    this.magma = magma;
    addArticle(magma);
  }

  void printSymbol(int x, int y) {
    var syn = '- ';
    for (int o = 0; o < count; o++) {
      if (cle[o].isOn(x, y)) {
        syn = cle[o].getSymbol();
      }
    }
    stdout.write('$syn');
  }

  void showMap() {
    ShowTitle();
    print("remaining : ");
    print(player!.getF());
    for (int y = 0; y < _height; y++) {
      for (int x = 0; x < _width; x++) {
        printSymbol(x, y);
      }
      ShowEnter();
    }
  }

  bool inMap(var _x, var _y) {
    return (_x >= 0 && _x < _width) && (_y >= 0 && _y < _height);
  }

  bool isMagma(int x, int y) {
    return magma!.isOn(x, y);
  }

  bool isDeadEnd(int x, int y) {
    for (int o = 0; o < count; o++) {
      if (cle[o] is DeadEnd && cle[o].isOn(x, y)) {
        return true;
      }
    }
    return false;
  }

  int newCraft(var x, var y) {
    for (int o = 0; o < count; o++) {
      if (cle[o] is Craft && cle[o].isOn(x, y)) {
        return 10;
      }
    }
    return 0;
  }

  int seeJackpot(int x, int y) {
    for (int o = 0; o < count; o++) {
      if (cle[o] is Jackpot && cle[o].isOn(x, y)) {
        Jackpot jackpot = cle[o] as Jackpot;     
        return jackpot.addjackpot();
      }
    }
    return 0;
  }
   int collJackpot(int x, int y) {
    for (int o = 0; o < count; o++) {
      if (cle[o] is Jackpot && cle[o].isOn(x, y)) {    
        cle.remove(cle[o]);
        return 1; 
      }
    }
    return 0;
  }


  void showMagma() {
    print(magma!.getSymbol());
  }

  void showPlayer() {
    print(player!.getSymbol());
  }

  void ShowTitle() {
    print("<<< This Is A Map >>>");
  }

  void ShowEnter() {
    print("");
  }

  void ShowLine() {
    print("-");
  }
}

import 'Article.dart';

class Magma extends Article {
  Magma(int _x, int _y) : super(_x, _y, 'M');

  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }
}

import 'Article.dart';

class DeadEnd extends Article {
  DeadEnd(int x, int y) : super(x, y, 'D');
}

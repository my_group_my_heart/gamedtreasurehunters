class Article {
  int x;
  int y;
  var symbol;

  Article(this.x, this.y, this.symbol);
  int getX() {
    return x;
  }

  int getY() {
    return y;
  }

  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }

  String getSymbol() {
    return symbol;
  }
}

import 'Article.dart';
import 'Map.dart';

class Player extends Article {
  int F;
  Map? map;
  int jackpot = 0;
  int jackpotitem = 0;

  Player(super.x, super.y, super.symbol, this.map, this.F);

  bool walk(var direction) {
    switch (direction) {
      case 'N': //ทิศ
      case 'w':
        if (walkN()) {
          return false;
        }
        break;
      case 'S':
      case 's':
        if (walkS()) {
          return false;
        }
        break;
      case 'E':
      case 'd':
        if (walkE()) {
          return false;
        }
        break;
      case 'W':
      case 'a':
        if (walkW()) {
          return false;
        }
        break;
      default:
        return false;
    }
    checkMagma();
    return true;
  }

  

 
  void checkJackpoy() {
    int jackpot = map!.seeJackpot(x, y);
    if (jackpot >= 0) {
      this.jackpot += jackpot;
    }
  }

  bool checkMagma() {
    if (map!.isMagma(x, y)) {}
    return false;
  }

  bool canWalk(int x, int y) {
    return map!.inMap(x, y) && !map!.isDeadEnd(x, y) && F > 0;
  }

  void CheckCraft() {
    int craft = map!.newCraft(x, y);
    if (craft > 0) {
      F += craft;
    }
  }

  int getF() {
    return F;
  }

  bool isOn(int x, int y) {
    if (this.x == x && this.y == y) {
      return true;
    }
    return false;
  }

  // void ShowtextMagma() {
  //   print("Found!!! X is $x Y is $y");
  //   print("Game Over");
  // }

  bool walkN() {
    CheckCraft();
    if (canWalk(x, y - 1)) {
      y = y - 1;
      F--;
    } else {
      return true;
    }
    return false;
  }

  bool walkS() {
    CheckCraft();
    if (canWalk(x, y + 1)) {
      y = y + 1;
      F--;
    } else {
      return true;
    }
    return false;
  }

  bool walkE() {
    CheckCraft();
    if (canWalk(x + 1, y)) {
      x = x + 1;
      F--;
    } else {
      return true;
    }
    return false;
  }

  bool walkW() {
    CheckCraft();
    if (canWalk(x - 1, y)) {
      x = x - 1;
      F--;
    } else {
      return true;
    }
    return false;
  }

  String printshow() {
    return "player" +
        "x : " +
        x.toString() +
        " , y : " +
        y.toString() +
        " F : " +
        F.toString() +
        " Jackpot : " +
        jackpot.toString();
  }
  
}
